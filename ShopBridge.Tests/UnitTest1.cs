﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShopBridge.Controllers;

namespace ShopBridge.Tests
{
    [TestClass]
    public class UnitTest1
    {
        ShopBridge.Controllers.ProductsController PR = new ProductsController();
        ShopBridge.Models.Tbl_Products MDL = new ShopBridge.Models.Tbl_Products();
        
        [TestMethod]
        public void TestGetProductAPI()
        {

            var output = PR.GetTbl_Products();

        }

        [TestMethod]
        public void testPostAPI()
        {
            var output = PR.PostTbl_Products(MDL);
        }

        [TestMethod]
        public void testUpdateAPI()
        {
            var output = PR.PutTbl_Products(MDL);
        }


        [TestMethod]
        public void testDeleteAPI()
        {
            var output = PR.DeleteTbl_Products(MDL);
        }
        



    }


}
