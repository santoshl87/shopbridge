﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ShopBridge.Models;
using Newtonsoft.Json;
using ShopBridge.BAL;


namespace ShopBridge.Controllers
{
    public class ProductsController : ApiController

    {
        private Db_ShopBridgeEntities db = new Db_ShopBridgeEntities();
        OutPutMsg output = new OutPutMsg();
        Cls_bl_Products clsBase = new Cls_bl_Products();
        Cls_bl_ProductCurd ClsCurd = new Cls_bl_ProductCurd();
        // GET: api/Products
        public IQueryable<Tbl_Products> GetTbl_Products()
        {
            return db.Tbl_Products;
        }

        // GET: api/Products/5
        [ResponseType(typeof(Tbl_Products))]
        public async Task<IHttpActionResult> GetTbl_Products(int id)
        {
            Tbl_Products tbl_Products = await db.Tbl_Products.FindAsync(id);
            if (tbl_Products == null)
            {
                return NotFound();
            }


            return Ok(tbl_Products);

        }
        // PUT: api/Products/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTbl_Products(Tbl_Products tbl_Products)
        {
            if (tbl_Products == null)
            {
                output.retMsg = "Invalid Request";
                output.retVal = "false";
                output.ScopeInt = 206;
                return Ok(output);
            }


            if (await clsBase.UserRights(tbl_Products) == true)
            {


                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }


                try
                {
                    output= await ClsCurd.Update(tbl_Products);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!clsBase.Tbl_ProductsExists(tbl_Products.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
            }

            return Ok(output);

        }
        // POST: api/Products
        [ResponseType(typeof(Tbl_Products))]
        public async Task<IHttpActionResult> PostTbl_Products(Tbl_Products tbl_Products)
        {
            if (tbl_Products == null)
            {
                output.retMsg = "Invalid Request";
                output.retVal = "false";
                output.retMsg = "204";
                return Ok(output);
            }


            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (await clsBase.UserRights(tbl_Products) == true)
            {

                if (await clsBase.ExpiryDate(tbl_Products) == true)
                {
                    if (await clsBase.ProductExists(tbl_Products) == true)

                    {
                        db.Tbl_Products.Add(tbl_Products);
                        string user = string.Empty;
                        try
                        {
                            output = await ClsCurd.Insert(tbl_Products);
                        }
                        catch (Exception ex) { }
                        return Ok(output);

                    }
                }
            }
            return Ok(output);


        }

        // DELETE: api/Products/5
        [ResponseType(typeof(Tbl_Products))]
        public async Task<IHttpActionResult> DeleteTbl_Products(Tbl_Products tbl_Products)
        {
            if (tbl_Products == null)
            {
                output.retMsg = "Invalid Request";
                output.retVal = "false";
                output.ScopeInt = 205;
                return Ok(output);
            }



            if (await clsBase.UserRights(tbl_Products) == true)
            {

                Tbl_Products tbl_Product = await db.Tbl_Products.FindAsync(tbl_Products.Id);
                if (tbl_Product == null)
                {
                    output.retMsg = "No Record Found";
                    output.retVal = "false";
                    output.ScopeInt = 204;

                    return Ok(output);
                }

                if (clsBase.Tbl_ProductsExists(tbl_Product.Id) == true)

                { 
                    var val = await ClsCurd.DeleteProduct(tbl_Product);

                }
                //db.Tbl_Products.Remove(tblId);
                //  await db.SaveChangesAsync();

                output.retMsg = "Record Deleted Successfully";
                output.retVal = "true";
                output.ScopeInt = 203;


                return Ok(output);


            }
            return Ok(output);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       
    }
}
