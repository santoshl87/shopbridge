﻿using ShopBridge.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ShopBridge.BAL
{
    public class Cls_bl_Products
    {
        private Db_ShopBridgeEntities db = new Db_ShopBridgeEntities();
        OutPutMsg output = new OutPutMsg();

        public bool Tbl_ProductsExists(int id)
        {
            return db.Tbl_Products.Count(e => e.Id == id) > 0;
        }
        public async Task<bool> ProductExists(Tbl_Products tbl)
        {
            Db_ShopBridgeEntities dbs = new Db_ShopBridgeEntities();
            using (dbs)
            {
                var query = dbs.Tbl_Products.Where(s => s.ProductName == tbl.ProductName).Count<Tbl_Products>();

                try
                {

                    if (query > 0)
                    {

                        output.retMsg = "Product Is allerady available in the database";
                        output.retVal = "Error";
                        output.ScopeInt = 202;
                        return false;


                    }

                }

                catch (Exception ex)
                {


                }
            }

            return true;


        }
  
        public async Task<bool> UpdateProduct(Tbl_Products tbl)
        {
            using (var dbss = new Db_ShopBridgeEntities())
            {
                Tbl_Products tblpr = new Tbl_Products()
                {

                    ModifiedDate = DateTime.Now,
                    ExpiryDate = tbl.ExpiryDate,
                    ProductName = tbl.ProductName,
                    Productdescription = tbl.Productdescription,
                    PurchasePrice = tbl.PurchasePrice,
                    SellingPrice = tbl.SellingPrice,
                    ProductSerialNo = tbl.ProductSerialNo,
                    CatagoryCode = tbl.CatagoryCode,
                    EntryBy = tbl.EntryBy,
                };
                var result = dbss.Tbl_Products.SingleOrDefault(b => b.Id == tbl.Id);
                if (result != null)
                {
                    try
                    {
                        dbss.Tbl_Products.Attach(tbl);
                        dbss.Entry(tblpr).State = EntityState.Modified;
                        dbss.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }

            Db_ShopBridgeEntities dbs = new Db_ShopBridgeEntities();
            using (dbs)
            {

                if (Tbl_ProductsExists(tbl.Id) == true)
                {

                    dbs.Database.ExecuteSqlCommand("delete from Tbl_Products where id =" + tbl.Id);
                    dbs.SaveChanges();
                    return true;

                }

                output.retMsg = "Record Not Exists in Database";
                output.retVal = "Error";
                output.ScopeInt = 206;

                return false;

            }

        }
        public async Task<bool> ExpiryDate(Tbl_Products tbl)
        {
            try
            {
                tbl_CatagoryMaster Tbl_CatagoryMaster = await db.tbl_CatagoryMaster.FindAsync(tbl.CatagoryCode);
                int isManded = Tbl_CatagoryMaster.RequireExpiryDate;

                if (isManded == 1)
                {
                    if (tbl.ExpiryDate.HasValue == true)
                    {
                        return true;

                    }

                }
            }
            catch (Exception ex)
            {

            }

            output.retMsg = "Expiry Date is Mandetory for this Product";
            output.retVal = "Error";
            output.ScopeInt = 201;

            return false;

        }
        public async Task<bool> UserRights(Tbl_Products tbl)
        {

            Db_ShopBridgeEntities dbs = new Db_ShopBridgeEntities();


            //Tbl_Products tbl_Products = await db.Tbl_Products.FindAsync(tbl.Id);
            using (dbs)
            {
                var query = dbs.tbl_Users.Where(s => s.username == tbl.EntryBy).FirstOrDefault<tbl_Users>();
                try
                {
                    string IsUserRole = query.role;
                    if (IsUserRole == "admin")
                    {
                        return true;

                    }

                }

                catch (Exception ex)
                {


                }
            }

            output.retMsg = "This User Not Allow to Perform this Action";
            output.retVal = "Error";
            output.ScopeInt = 200;
            return false;



        }

    }
}