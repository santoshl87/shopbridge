﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ShopBridge;
using ShopBridge.Models;
using System.Threading.Tasks;
using System.Data.Entity.Core.Objects;

namespace ShopBridge.BAL
{




    public class Cls_bl_ProductCurd
    {
        private Db_ShopBridgeEntities db = new Db_ShopBridgeEntities();
        private Db_ShopBridgeEntities dUpdateb = new Db_ShopBridgeEntities();

        OutPutMsg output = new OutPutMsg();
        Cls_bl_Products ClsBase = new Cls_bl_Products();

        public async Task<OutPutMsg> Insert(Tbl_Products tbl_products)

        {
            try
            {

                var retValNameParameter = output.retVal != null ?
                 new ObjectParameter("retVal", output.retVal) :
                 new ObjectParameter("retVal", typeof(string));
                var retMsgNameParameter = output.retMsg != null ?
                 new ObjectParameter("retMsg", output.retMsg) :
                 new ObjectParameter("retMsg", typeof(string));
                var scopeIntNameParameter = output.ScopeInt.ToString() != null ?
                 new ObjectParameter("scopeInt", output.ScopeInt.ToString()) :
                 new ObjectParameter("scopeInt", typeof(int));
                var s = db.SP_INST_Tbl_Products(tbl_products.ProductName, tbl_products.Productdescription, tbl_products.PurchasePrice, tbl_products.SellingPrice, tbl_products.ProductSerialNo, tbl_products.ExpiryDate, tbl_products.EntryBy, tbl_products.Status, tbl_products.CatagoryCode, retVal: retValNameParameter, retMsg: retMsgNameParameter, scopeInt: scopeIntNameParameter);
                output.retMsg = retMsgNameParameter.Value.ToString();
                output.retVal = retValNameParameter.Value.ToString();
                output.ScopeInt = Convert.ToInt32(scopeIntNameParameter.Value.ToString());
                return output;
            }

            catch
            {

            }

            return output;



        }
        public async Task<OutPutMsg> Update(Tbl_Products tbl_products)

        {
            try
            {

                Db_ShopBridgeEntities dbUpdate = new Db_ShopBridgeEntities();
                var retValNameParameter = output.retVal != null ?
                 new ObjectParameter("retVal", output.retVal) :
                 new ObjectParameter("retVal", "0");
                var retMsgNameParameter = output.retMsg != null ?
                 new ObjectParameter("retMsg", output.retMsg) :
                 new ObjectParameter("retMsg", "0");
                var scopeIntNameParameter = output.ScopeInt.ToString() != null ?
                 new ObjectParameter("scopeInt", output.ScopeInt) :
                 new ObjectParameter("scopeInt", 0);
                string retval = retValNameParameter.Value.ToString();
                string retMsg = retMsgNameParameter.Value.ToString();
                int scopeInt = Convert.ToInt16(scopeIntNameParameter.Value.ToString());
                dbUpdate.Database.ExecuteSqlCommand("exec Sp_Upd_Tbl_Products " + "'" + tbl_products.Id + "','" + tbl_products.ProductName + "','" + tbl_products.Productdescription + "','" + tbl_products.PurchasePrice + "','" + tbl_products.SellingPrice + "','" + tbl_products.ProductSerialNo + "','" + tbl_products.ExpiryDate + "','" + tbl_products.EntryBy + "','" + tbl_products.Status + "','" + tbl_products.CatagoryCode + "','" + retval + "','" + retMsg + "','" + scopeInt + "'");
                output.retMsg = "Record Updated Successfully";
                output.retVal = "true";
                output.ScopeInt = tbl_products.Id;
                return output;
            }

            catch (Exception ex)
            {
                output.retMsg = ex.Message;
                output.retVal = "false";
                output.ScopeInt = 209;
                return output;
            }

            return output;



        }
        public async Task<bool> DeleteProduct(Tbl_Products tbl)
        {

            Db_ShopBridgeEntities dbs = new Db_ShopBridgeEntities();
            using (dbs)
            {

                dbs.Database.ExecuteSqlCommand("delete from Tbl_Products where id =" + tbl.Id);
                dbs.SaveChanges();

                output.retMsg = "Record Deleted From Database";
                output.retVal = "Success";
                output.ScopeInt = 207;

                return true;


            }

            output.retMsg = "Record Not Exists in Database";
            output.retVal = "Error";
            output.ScopeInt = 206;

            return false;

        }

    }

}
