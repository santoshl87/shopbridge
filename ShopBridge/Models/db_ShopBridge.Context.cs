﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ShopBridge.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;

    using System.Linq;

    public partial class Db_ShopBridgeEntities : DbContext
    {
        public Db_ShopBridgeEntities()
            : base("name=Db_ShopBridgeEntities")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }

        public DbSet<Tbl_Products> Tbl_Products { get; set; }
        public DbSet<tbl_CatagoryMaster> tbl_CatagoryMaster { get; set; }
        public DbSet<tbl_Users> tbl_Users { get; set; }

        public virtual int SP_INST_Tbl_Products(string productName, string productDescription, Nullable<decimal> purchasePrice, Nullable<decimal> sellingPrice, string productSerialNo, Nullable<System.DateTime> expiryDate, string entryBy, Nullable<int> status, Nullable<int> catagoryCode, ObjectParameter retVal, ObjectParameter retMsg, ObjectParameter scopeInt)
        {
            var productNameParameter = productName != null ?
                new ObjectParameter("productName", productName) :
                new ObjectParameter("productName", typeof(string));

            var productDescriptionParameter = productDescription != null ?
                new ObjectParameter("ProductDescription", productDescription) :
                new ObjectParameter("ProductDescription", typeof(string));

            var purchasePriceParameter = purchasePrice.HasValue ?
                new ObjectParameter("PurchasePrice", purchasePrice) :
                new ObjectParameter("PurchasePrice", typeof(decimal));

            var sellingPriceParameter = sellingPrice.HasValue ?
                new ObjectParameter("SellingPrice", sellingPrice) :
                new ObjectParameter("SellingPrice", typeof(decimal));

            var productSerialNoParameter = productSerialNo != null ?
                new ObjectParameter("ProductSerialNo", productSerialNo) :
                new ObjectParameter("ProductSerialNo", typeof(string));

            var expiryDateParameter = expiryDate.HasValue ?
                new ObjectParameter("expiryDate", expiryDate) :
                new ObjectParameter("expiryDate", typeof(System.DateTime));

            var entryByParameter = entryBy != null ?
                new ObjectParameter("EntryBy", entryBy) :
                new ObjectParameter("EntryBy", typeof(string));

            var statusParameter = status.HasValue ?
                new ObjectParameter("status", status) :
                new ObjectParameter("status", typeof(int));

            var catagoryCodeParameter = catagoryCode.HasValue ?
                new ObjectParameter("catagoryCode", catagoryCode) :
                new ObjectParameter("catagoryCode", typeof(int));

            ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SP_INST_Tbl_Products", productNameParameter, productDescriptionParameter, purchasePriceParameter, sellingPriceParameter, productSerialNoParameter, expiryDateParameter, entryByParameter, statusParameter, catagoryCodeParameter, retVal, retMsg, scopeInt);

            OutPutMsg output = new OutPutMsg();

            output.retMsg = retMsg.Value.ToString();
            output.retVal = retVal.Value.ToString();
            output.ScopeInt = Convert.ToInt16(scopeInt.Value.ToString());
            return 1;
        }
        public virtual int Sp_Upd_Tbl_Products(Nullable<int> id, string productName, string productdescription, Nullable<decimal> purchasePrice, Nullable<decimal> sellingPrice, string productSerialNo, Nullable<System.DateTime> expiryDate, string entryBy, Nullable<int> status, Nullable<int> catagoryCode, ObjectParameter retval, ObjectParameter retMsg, ObjectParameter scopeInt)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("id", id) :
                new ObjectParameter("id", typeof(int));

            var productNameParameter = productName != null ?
                new ObjectParameter("ProductName", productName) :
                new ObjectParameter("ProductName", typeof(string));

            var productdescriptionParameter = productdescription != null ?
                new ObjectParameter("Productdescription", productdescription) :
                new ObjectParameter("Productdescription", typeof(string));

            var purchasePriceParameter = purchasePrice.HasValue ?
                new ObjectParameter("PurchasePrice", purchasePrice) :
                new ObjectParameter("PurchasePrice", typeof(decimal));

            var sellingPriceParameter = sellingPrice.HasValue ?
                new ObjectParameter("SellingPrice", sellingPrice) :
                new ObjectParameter("SellingPrice", typeof(decimal));

            var productSerialNoParameter = productSerialNo != null ?
                new ObjectParameter("ProductSerialNo", productSerialNo) :
                new ObjectParameter("ProductSerialNo", typeof(string));

            var expiryDateParameter = expiryDate.HasValue ?
                new ObjectParameter("ExpiryDate", expiryDate) :
                new ObjectParameter("ExpiryDate", typeof(System.DateTime));

            var entryByParameter = entryBy != null ?
                new ObjectParameter("EntryBy", entryBy) :
                new ObjectParameter("EntryBy", typeof(string));

            var statusParameter = status.HasValue ?
                new ObjectParameter("Status", status) :
                new ObjectParameter("Status", typeof(int));

            var catagoryCodeParameter = catagoryCode.HasValue ?
                new ObjectParameter("CatagoryCode", catagoryCode) :
                new ObjectParameter("CatagoryCode", typeof(int));

          var outInt=  ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("Sp_Upd_Tbl_Products", idParameter, productNameParameter, productdescriptionParameter, purchasePriceParameter, sellingPriceParameter, productSerialNoParameter, expiryDateParameter, entryByParameter, statusParameter, catagoryCodeParameter, retval, retMsg, scopeInt);

            OutPutMsg output = new OutPutMsg();
            output.retMsg = retMsg.Value.ToString();
            output.retVal = retval.Value.ToString();
            output.ScopeInt = Convert.ToInt16(scopeInt.Value.ToString());
           

            return outInt;

        }
    }
}
